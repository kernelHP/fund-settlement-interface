package com.yuanheng100.settlement.service;

import com.yuanheng100.settlement.model.customer.CustomerBindCard;
import com.yuanheng100.settlement.model.customer.CustomerLogin;
import com.yuanheng100.settlement.model.customer.CustomerRegister;

/**
 * 与客户相关的操作接口，开户绑卡等操作
 * @author tianwei
 *
 */
public interface ICustomerService {
    
    /**
     * 客户开户接口
     * @param register
     * @return
     */
    CustomerRegister customerRegister(CustomerRegister register);

    /**
     * 同步客户信息<br>
     * 首信易：客户同步<br>
     * 汇付天下：客户信息修改，删除银行卡
     * @param register
     * @return
     */
    CustomerRegister syncCustomerAccount(CustomerRegister register);
    
    /**
     * 查询客户
     * @param register
     * @return
     */
    CustomerRegister customerQuery(CustomerRegister register);
    
    /**
     * 客户绑卡接口
     * @param bindCard
     * @return
     */
    CustomerBindCard customerBindCard(CustomerBindCard bindCard);
    
    /**
     * 客户登录<br>
     * 浏览器调用，无返回
     * @param customerLogin
     */
    void customerLogin(CustomerLogin customerLogin);
    
}
